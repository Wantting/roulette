/**
 * Roulettewheel
 */
import java.util.Random;
public class RouletteWheel {
 
    private Random rand = new Random();
    private int numLastSpin = 0;

    public void spin() {
        this.numLastSpin = rand.nextInt(37);
    }
    public int getValue() {
        return this.numLastSpin;
    }
}